#pragma once
#include "GameState.h"
#include "ResourceManager.h"
#include "Point.h"
#include "enumObstacles.h"
#include "astar.h"
#include "AllUnits.h"
#include "UserInterface.h"
#include "Constants.h"
#include "MapObjects.h"
#include "Meteor.h"

//���� � ���� ��������� ����������. 
//������ ��������� ������� ��������� ���������.  
//������� ������� ������, ��� ������ ��������
//������ �� ��, � ��������� ������������� �� ��������� , ����� ��������� ����������� �������

int amountOfChris = 3;
int amountOfBudd = 3;
bool drawTheRune = true;

#define watch(x) cout << (#x) << " is " << (x) << endl

//���� �������� ���, ����� ������� ������ ������, �������������� ��� 
//����� 2 ,���� 3 , 1 ������(������ �������� �� 100 ��) , 1 ��� ������(��� �������)  
//�������� ��� ������ ������
//�������� ������ ����
//��������
//������������� � ����-����� ���, ��������� world1,�� �������� ����� ������ �� ���� 

extern int spawnHeroes(Unit* unit, int fieldB[FIELDSIZEX][FIELDSIZEY], OBS_TYPE obstacles[FIELDSIZEX][FIELDSIZEY]);
extern int wayToGs(int fieldB[FIELDSIZEX][FIELDSIZEY], int i, int j, Point g1, Point g2, Point g3, Point g4);
extern void mapGeneration();
			
class MainGameState : public GameState {

private:

	bool mouseMovementAllow = true;
	bool jesusVictory = false;
	bool buddhaVictory = false;
	bool cast = false;
	bool hoodDraw = false;
	bool readyToGo = false;
	bool attackMovement = false;
	bool attackTheTowerMovement = false;
	bool damageDraw = false;
	int camX = 0, camY = 0;
	int generationType = 1;
	int cellSize = 52; 
	int start = 0;
	int fieldB[FIELDSIZEX][FIELDSIZEY];
	int unitsField[FIELDSIZEX][FIELDSIZEY];
	int playersTurn = 0;
	int startX, startY, endX, endY;
	int startTheWay = 0;
	int currentCellX = 0;
	int currentCellY = 0;
	int cirlcesAmount = 0;
	int timerGoingTmp = 0;
	int clickedCellX = 0;
	int clickedCellY = 0;
	int meteorClickedCellX = 0;
	int meteorClickedCellY = 0;
	int meteorCurrentCellX = 0;
	int meteorCurrentCellY = 0;
	float timer = 0;
	float damageTimer = 0;
	sf::RectangleShape *rect;
	sf::RectangleShape* rectField;
	sf::RectangleShape* rectObstacle;
	sf::RectangleShape* unitsRect;
	sf::RectangleShape* rectMapObj;
	sf::RectangleShape* meteorRect;
	sf::RectangleShape* meteorAOErect;
	sf::RectangleShape* castleAreaRect;
	sf::RectangleShape* damageIconRect;
	sf::CircleShape *nodeShape;
	sf::Text *text = new sf::Text();
	sf::Text *text2 = new sf::Text();
	Point g1, g2, g3, g4;
	Point obsidian1, obsidian2;
	OBS_TYPE obstacles[FIELDSIZEX][FIELDSIZEY];
	sf::Font *font = new sf::Font();
	Unit* units[AMOUNTOFUNITS];
	Unit* selectedUnit = nullptr;
	Unit* selectedUnit2 = nullptr;
	MapObject* selectedTower = nullptr;
	MapObject* mapObjects[3];
	sf::Clock clock;
	UserInterface *userInterface;
	astar::World *world = nullptr;
	std::vector<astar::Node*> path;
	Meteor *meteor = nullptr;

	void mapGeneration() { //��������� ���� �����
		generationType = rand() % 2;
		//0-��������� ������ 1-������ 2-����

		int mapX, mapY, mapX2, mapY2, mapX3, mapY3, pointStyle, pointStyle2, pointStyle3;
		int mapX4, mapY4, pointStyle4;

		if (generationType == 0) {//��������� ������
			mapX = rand() % 100;
			mapY = rand() % 100;
			pointStyle = rand() % 3; //0 ����� 1-����� 2-����
			g1 = Point(mapX, mapY, pointStyle);


			mapX2 = rand() % 100;
			mapY2 = rand() % 100;
			pointStyle2 = rand() % 3;
			g2 = Point(mapX2, mapY2, pointStyle2);

			mapX3 = rand() % 100;
			mapY3 = rand() % 100;
			pointStyle3 = rand() % 3;
			g3 = Point(mapX3, mapY3, pointStyle3);

			mapX4 = rand() % 100;
			mapY4 = rand() % 100;
			pointStyle4 = rand() % 3;
			g4 = Point(mapX4, mapY4, pointStyle4);
		}

		if (generationType == 1) {//������
			mapX = 10 + rand() % 40;
			mapY = 10 + rand() % 40;
			pointStyle = rand() % 2; //0 ����� 1-����� 
			g1 = Point(mapX, mapY, pointStyle);

			mapX2 = 50 + rand() % 40;
			mapY2 = 10 + rand() % 40;
			pointStyle2 = rand() % 2;
			g2 = Point(mapX2, mapY2, pointStyle2);

			mapX3 = 10 + rand() % 40;
			mapY3 = 50 + rand() % 40;
			pointStyle3 = rand() % 2;
			g3 = Point(mapX3, mapY3, pointStyle3);

			mapX4 = 50 + rand() % 40;
			mapY4 = 50 + rand() % 40;
			pointStyle4 = rand() % 2;
			g4 = Point(mapX4, mapY4, pointStyle4);
		}

	}

	int wayToGs(int fieldB[FIELDSIZEX][FIELDSIZEY], int i, int j, Point g1, Point g2, Point g3, Point g4) {

		int result;

		if (generationType == 0) {
			int wayToG1 = sqrt((j - g1.getY())*(j - g1.getY()) + (i - g1.getX())*(i - g1.getX()));
			int wayToG2 = sqrt((j - g2.getY())*(j - g2.getY()) + (i - g2.getX())*(i - g2.getX()));
			int wayToG3 = sqrt((j - g3.getY())*(j - g3.getY()) + (i - g3.getX())*(i - g3.getX()));
			int wayToG4 = sqrt((j - g4.getY())*(j - g4.getY()) + (i - g4.getX())*(i - g4.getX()));

			//0 ����� 1-����� 2-����

			if (wayToG1 < wayToG2 && wayToG1 < wayToG3 && wayToG1 < wayToG4) {
				if (g1.getStyle() == 2) {
					fieldB[i][j] = -1;
				}
				if (g1.getStyle() == 0) {
					fieldB[i][j] = 1;
				}
				if (g1.getStyle() == 1) {
					fieldB[i][j] = 3;
				}
				return g1.getStyle();

			}

			if (wayToG2 < wayToG1 && wayToG2 < wayToG3 && wayToG2 < wayToG4) {
				if (g2.getStyle() == 2) {
					fieldB[i][j] = -1;
				}
				if (g2.getStyle() == 0) {
					fieldB[i][j] = 1;
				}
				if (g2.getStyle() == 1) {
					fieldB[i][j] = 3;
				}
				return g2.getStyle();
			}

			if (wayToG3 < wayToG2 && wayToG3 < wayToG1 && wayToG3 < wayToG4) {
				if (g3.getStyle() == 2) {
					fieldB[i][j] = -1;
				}
				if (g3.getStyle() == 0) {
					fieldB[i][j] = 1;
				}
				if (g3.getStyle() == 1) {
					fieldB[i][j] = 3;
				}
				return g3.getStyle();
			}

			if (wayToG4 < wayToG2 && wayToG4 < wayToG3 && wayToG4 < wayToG1) {
				if (g4.getStyle() == 2) {
					fieldB[i][j] = -1;
				}
				if (g4.getStyle() == 0) {
					fieldB[i][j] = 1;
				}
				if (g4.getStyle() == 1) {
					fieldB[i][j] = 3;
				}
				return g4.getStyle();
			}

			int random;

			if (wayToG1 == wayToG2) {
				random = rand() % 2;
				if (random == 0) {
					if (g1.getStyle() == 2) {
						fieldB[i][j] = -1;
					}
					if (g1.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g1.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g1.getStyle();
				}
				else {
					if (g2.getStyle() == 2) {
						fieldB[i][j] = -1;
					}
					if (g2.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g2.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g2.getStyle();
				}
			}

			if (wayToG1 == wayToG3) {
				random = rand() % 2;
				if (random == 0) {
					if (g1.getStyle() == 2) {
						fieldB[i][j] = -1;
					}
					if (g1.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g1.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g1.getStyle();
				}
				else {
					if (g3.getStyle() == 2) {
						fieldB[i][j] = -1;
					}
					if (g3.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g3.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g3.getStyle();
				}
			}

			if (wayToG1 == wayToG4) {
				random = rand() % 2;
				if (random == 0) {
					if (g1.getStyle() == 2) {
						fieldB[i][j] = -1;
					}
					if (g1.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g1.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g1.getStyle();
				}
				else {
					if (g4.getStyle() == 2) {
						fieldB[i][j] = -1;
					}
					if (g4.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g4.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g4.getStyle();
				}
			}

			if (wayToG4 == wayToG2) {
				random = rand() % 2;
				if (random == 0) {
					if (g4.getStyle() == 2) {
						fieldB[i][j] = -1;
					}
					if (g4.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g4.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g4.getStyle();
				}
				else {
					if (g2.getStyle() == 2) {
						fieldB[i][j] = -1;
					}
					if (g2.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g2.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g2.getStyle();
				}
			}

			if (wayToG3 == wayToG2) {
				random = rand() % 2;
				if (random == 0) {
					if (g3.getStyle() == 2) {
						fieldB[i][j] = -1;
					}
					if (g3.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g3.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g3.getStyle();
				}
				else {
					if (g2.getStyle() == 2) {
						fieldB[i][j] = -1;
					}
					if (g2.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g2.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g2.getStyle();
				}
			}

			if (wayToG3 == wayToG4) {
				random = rand() % 2;
				if (random == 0) {
					if (g3.getStyle() == 2) {
						fieldB[i][j] = -1;
					}
					if (g3.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g3.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g3.getStyle();
				}
				else {
					if (g4.getStyle() == 2) {
						fieldB[i][j] = -1;
					}
					if (g4.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g4.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g4.getStyle();
				}
			}
		}

		if (generationType == 1) {

			int wayToG1 = sqrt((j - g1.getY())*(j - g1.getY()) + (i - g1.getX())*(i - g1.getX()));
			int wayToG2 = sqrt((j - g2.getY())*(j - g2.getY()) + (i - g2.getX())*(i - g2.getX()));
			int wayToG3 = sqrt((j - g3.getY())*(j - g3.getY()) + (i - g3.getX())*(i - g3.getX()));
			int wayToG4 = sqrt((j - g4.getY())*(j - g4.getY()) + (i - g4.getX())*(i - g4.getX()));

			int shortestWayToWater = 1234;
			for (int m = 0; m < FIELDSIZEY; m++) {//�������� ���������� �� ����� �� ��������� ���� ������ � �����
				int wayToWaterTop = sqrt(i*i + (j - m)*(j - m));
				int wayToWaterBot = sqrt((i - FIELDSIZEX - 1)*(i - FIELDSIZEX - 1) + (j - m)*(j - m));

				if (wayToWaterBot < shortestWayToWater && wayToWaterBot <= wayToWaterTop) {
					shortestWayToWater = wayToWaterBot;
				}
				if (wayToWaterTop < shortestWayToWater && wayToWaterTop <= wayToWaterBot) {
					shortestWayToWater = wayToWaterTop;
				}
			}

			for (int n = 0; n < FIELDSIZEX; n++) {
				int wayToWaterLeft = sqrt((i - n)*(i - n) + j * j);
				int wayToWaterRight = sqrt((i - n)*(i - n) + (j - FIELDSIZEY - 1)*(j - FIELDSIZEY - 1));

				if (wayToWaterLeft < shortestWayToWater && wayToWaterLeft <= wayToWaterRight) {
					shortestWayToWater = wayToWaterLeft;
				}
				if (wayToWaterRight < shortestWayToWater && wayToWaterRight < wayToWaterLeft) {
					shortestWayToWater = wayToWaterRight;
				}
			}
			//0 ����� 1-����� 2-����
			if (shortestWayToWater < wayToG1 && shortestWayToWater < wayToG2 && shortestWayToWater < wayToG3 && shortestWayToWater < wayToG4) {
				return 2;
				fieldB[i][j] = -1;
			}

			if (wayToG1 < wayToG2 && wayToG1 < wayToG3 && wayToG1 < wayToG4) {

				if (g1.getStyle() == 0) {
					fieldB[i][j] = 1;
				}
				if (g1.getStyle() == 1) {
					fieldB[i][j] = 3;
				}


				return g1.getStyle();
			}

			if (wayToG2 < wayToG1 && wayToG2 < wayToG3 && wayToG2 < wayToG4) {
				if (g2.getStyle() == 0) {
					fieldB[i][j] = 1;
				}
				if (g2.getStyle() == 1) {
					fieldB[i][j] = 3;
				}
				return g2.getStyle();
			}

			if (wayToG3 < wayToG2 && wayToG3 < wayToG1 && wayToG3 < wayToG4) {
				if (g3.getStyle() == 0) {
					fieldB[i][j] = 1;
				}
				if (g3.getStyle() == 1) {
					fieldB[i][j] = 3;
				}
				return g3.getStyle();
			}

			if (wayToG4 < wayToG2 && wayToG4 < wayToG3 && wayToG4 < wayToG1) {
				if (g4.getStyle() == 0) {
					fieldB[i][j] = 1;
				}
				if (g4.getStyle() == 1) {
					fieldB[i][j] = 3;
				}
				return g4.getStyle();
			}

			int random;
			if (wayToG1 == wayToG2) {
				random = rand() % 2;
				if (random == 0) {
					if (g1.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g1.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g1.getStyle();
				}
				else {
					if (g2.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g2.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g2.getStyle();
				}
			}

			if (wayToG1 == wayToG3) {
				random = rand() % 2;
				if (random == 0) {
					if (g1.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g1.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g1.getStyle();
				}
				else {
					if (g3.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g3.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g3.getStyle();
				}
			}

			if (wayToG1 == wayToG4) {
				random = rand() % 2;
				if (random == 0) {
					if (g1.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g1.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g1.getStyle();
				}
				else {
					if (g4.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g4.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g4.getStyle();
				}
			}

			if (wayToG4 == wayToG2) {
				random = rand() % 2;
				if (random == 0) {
					if (g4.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g4.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g4.getStyle();
				}
				else {
					if (g2.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g2.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g2.getStyle();
				}
			}

			if (wayToG3 == wayToG2) {
				random = rand() % 2;
				if (random == 0) {
					if (g3.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g3.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g3.getStyle();
				}
				else {
					if (g2.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g2.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g2.getStyle();
				}
			}

			if (wayToG3 == wayToG4) {
				random = rand() % 2;
				if (random == 0) {
					if (g3.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g3.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g3.getStyle();
				}
				else {
					if (g4.getStyle() == 0) {
						fieldB[i][j] = 1;
					}
					if (g4.getStyle() == 1) {
						fieldB[i][j] = 3;
					}
					return g4.getStyle();
				}
			}


		}

	}

	int spawnHeroes(Unit* unit, int fieldB[FIELDSIZEX][FIELDSIZEY], OBS_TYPE obstacles[FIELDSIZEX][FIELDSIZEY], MapObject *mapObjects[3]) {

		while (1) {
			int a = rand() % FIELDSIZEX;
			int b = rand() % FIELDSIZEY;

			if ((a >= mapObjects[0]->getX() && a <= mapObjects[0]->getX() + 2 && b >= mapObjects[0]->getX() && b <= mapObjects[0]->getX() + 2) ||
				(a >= mapObjects[1]->getX() && a <= mapObjects[1]->getX() + 2 && b >= mapObjects[1]->getX() && b <= mapObjects[1]->getX() + 2)) {
				continue;
			}else if (fieldB[a][b] != -1 && fieldB[a][b] != 6 && obstacles[a][b] == (int)0) {
				unit->setX(a);
				unit->setY(b);
				return 0;
			}
		}
	}

	
public:

	~MainGameState() {
		delete world;
		delete rect;
		delete text;
		delete text2;
		delete rectField;
		delete rect;
		
		for (int i = 0; i < AMOUNTOFUNITS; i++) {
			delete units[i];
		}
		
		delete unitsRect;
		delete nodeShape;
		delete userInterface;
	}

	void init() {
		if (world != nullptr) {
			delete world;
		}

		world = new astar::World(FIELDSIZEX, FIELDSIZEY);

		mapGeneration();
	    //g1, g2, g3,g4 are map generation points
		if (g1.getStyle() == 2) {
			fieldB[g1.getX()][g1.getY()] = -1;
		}
		if (g2.getStyle() == 2) {
			fieldB[g2.getX()][g2.getY()] = -1;
		}
		if (g3.getStyle() == 2) {
			fieldB[g3.getX()][g3.getY()] = -1;
		}
		if (g4.getStyle() == 2) {
			fieldB[g4.getX()][g4.getY()] = -1;
		}

		if (generationType == 0) {
			if (g1.getStyle() == 2) {
				fieldB[g1.getX()][g1.getY()] = -1;
			}
			if (g1.getStyle() == 1) {
				fieldB[g1.getX()][g1.getY()] = 3;
			}
			if (g1.getStyle() == 0) {
				fieldB[g1.getX()][g1.getY()] = 1;
			}
			

			if (g2.getStyle() == 2) {
				fieldB[g2.getX()][g2.getY()] = -1;
			}
			if (g2.getStyle() == 1) {
				fieldB[g2.getX()][g2.getY()] = 3;
			}
			if (g2.getStyle() == 0) {
				fieldB[g2.getX()][g2.getY()] = 1;
			}

			if (g3.getStyle() == 2) {
				fieldB[g3.getX()][g3.getY()] = -1;
			}
			if (g3.getStyle() == 1) {
				fieldB[g3.getX()][g3.getY()] = 3;
			}
			if (g3.getStyle() == 0) {
				fieldB[g3.getX()][g3.getY()] = 1;
			}

			if (g4.getStyle() == 2) {
				fieldB[g4.getX()][g4.getY()] = -1;
			}
			if (g4.getStyle() == 1) {
				fieldB[g4.getX()][g4.getY()] = 3;
			}
			if (g4.getStyle() == 0) {
				fieldB[g4.getX()][g4.getY()] = 1;
			}
		}

		if (generationType == 1) { // 0 ����� 1 ����� 2 ����

			for (int i = 0; i < FIELDSIZEY; i++) {//��������� ����� ���� � ���
				fieldB[0][i] = -1;
				fieldB[FIELDSIZEX - 1][i] = -1;
				fieldB[i][0] = -1;
				fieldB[i][FIELDSIZEY - 1] = -1;
			}
		}

		for (int i = 0; i < FIELDSIZEX; i++) {
			for (int j = 0; j < FIELDSIZEY; j++) {
				int tmp = wayToGs(fieldB, i, j, g1, g2, g3, g4);

				if (tmp == 0) {
					fieldB[i][j] = 1;
				}
				if (tmp == 1) {
					fieldB[i][j] = 3;
				}
				if (tmp == 2) {
					fieldB[i][j] = -1;
				}
			}
		}

		rectField = new sf::RectangleShape();
		rectField->setSize(sf::Vector2f(cellSize, cellSize));
		
		meteorRect = new sf::RectangleShape();
		meteorRect->setSize(sf::Vector2f(METEOR_SIZE, METEOR_SIZE));
		meteorRect->setTexture(ResourceManager::sharedInstance().getTextureById(METEOR));
		meteorRect->setOrigin(sf::Vector2f(156.22, 146.9));

		meteorAOErect = new sf::RectangleShape();
		meteorAOErect->setSize(sf::Vector2f(cellSize * 3, cellSize * 3));
		meteorAOErect->setOrigin(sf::Vector2f(cellSize * 3 / 2, cellSize * 3 / 2));
		meteorAOErect->setFillColor(sf::Color(255, 0, 0, 128));

		rectObstacle = new sf::RectangleShape();
		rectObstacle->setSize(sf::Vector2f(cellSize, cellSize));

		rect = new sf::RectangleShape(); //��� ����
		rect->setPosition(sf::Vector2f(0, 0));
		rect->setSize(sf::Vector2f(1024, 600));
		rect->setTexture(ResourceManager::sharedInstance().getTextureById(TEXBMenu));

		damageIconRect = new sf::RectangleShape();
		damageIconRect->setSize(sf::Vector2f(cellSize, cellSize));
		damageIconRect->setTexture(ResourceManager::sharedInstance().getTextureById(ATTACKRECT));

		sf::RectangleShape *background = new sf::RectangleShape();
		sf::Texture *tex2 = new sf::Texture();

		background->setTexture(ResourceManager::sharedInstance().getTextureById(TEXBMain));
		background->setSize(sf::Vector2f(800, 600));
		background->setPosition(0, 0);

		for (int i = 0; i < FIELDSIZEX; i++) {
			for (int j = 0; j < FIELDSIZEY; j++) {
				obstacles[i][j] = (OBS_TYPE)0;
			}
		}

		for (int i = 0; i < FIELDSIZEX; i++) {
			for (int j = 0; j < FIELDSIZEY; j++) {


				if (fieldB[i][j] == -1) {//����������� �������� �� ����
					int randomGenerationOfWaterLilly = rand() % 40;
					if (randomGenerationOfWaterLilly == 0) {
						obstacles[i][j] = (OBS_TYPE)2;
					}
				}


				if (fieldB[i][j] == 1) { //����������� ������� � ����� 
					int randomGenerationAtDirt = rand() % 15;
					int randTypeOfObsAtDirt = rand() % 2;


					if (randomGenerationAtDirt == 0) {


						if (randTypeOfObsAtDirt == 0) { //������ ������
							obstacles[i][j] = (OBS_TYPE)4;
						}
						else {
							obstacles[i][j] = (OBS_TYPE)1;
						}
					}


				}


				if (fieldB[i][j] == 3) {//����������� ��������-���� �� �����


					int randomGenerationAtSand = rand() % 40;


					if (randomGenerationAtSand == 0) {
						obstacles[i][j] = (OBS_TYPE)3;
					}

				}
			}
		}


		if (path.size() > 0) {
			for (int n = path.size() - 1; n >= 0; n--) {
				astar::Node *node = path.at(n);

				fieldB[node->x][node->y] = 6;
			}
		}

		int tmpObjectCounterGenerator = 0;

		while (tmpObjectCounterGenerator < 3) {
			int tmpMapObjX = rand() % FIELDSIZEX;
			int tmpMapObjY = rand() % FIELDSIZEY;

			if (fieldB[tmpMapObjX][tmpMapObjY] != -1 && obstacles[tmpMapObjX][tmpMapObjY] == 0) {
				MapObject* tmp = new MapObject(tmpMapObjX, tmpMapObjY, tmpObjectCounterGenerator);
				mapObjects[tmpObjectCounterGenerator] = tmp;
				tmpObjectCounterGenerator++;
			}

		}

		//Setup pathfinding world 
		for (int i = 0; i < FIELDSIZEX; i++) {
			for (int j = 0; j < FIELDSIZEY; j++) {
				world->setTileMovementCost(i, j, 10 + fieldB[i][j] * 10);

				//Water 
				if (fieldB[i][j] == -1) {
					world->setObstacle(i, j);
				}

				if (obstacles[i][j] != 0) {
					world->setObstacle(i, j);
				}

			}
		}


	
				world->setObstacle(mapObjects[1]->getX() + 1 , mapObjects[1]->getY() + 1 );
				world->setObstacle(mapObjects[0]->getX() + 1,  mapObjects[0]->getY() + 1);

		castleAreaRect = new sf::RectangleShape();
		castleAreaRect->setSize(sf::Vector2f(cellSize * 3, cellSize * 3));

		Unit* warriorRadiant = new Warrior(true); spawnHeroes(warriorRadiant, fieldB, obstacles, mapObjects); unitsField[warriorRadiant->getX()][warriorRadiant->getY()] = WARRIORRADIANT;
		Unit* archerRadiant = new Archer(true); spawnHeroes(archerRadiant, fieldB, obstacles,  mapObjects); unitsField[archerRadiant->getX()][archerRadiant->getY()] = ARCHERRADIANT;
		Unit* mageRadiant = new Mage(true); spawnHeroes(mageRadiant, fieldB, obstacles,  mapObjects); unitsField[mageRadiant->getX()][mageRadiant->getY()] = MAGERADIANT;
		Unit* warriorDire = new Warrior(false); spawnHeroes(warriorDire, fieldB, obstacles,  mapObjects); unitsField[warriorDire->getX()][warriorDire->getY()] = WARRIORDIRE;
		Unit* archerDire = new Archer(false); spawnHeroes(archerDire, fieldB, obstacles,  mapObjects); unitsField[archerDire->getX()][archerDire->getY()] = ARCHERDIRE;
		Unit* mageDire = new Mage(false); spawnHeroes(mageDire, fieldB, obstacles,  mapObjects); unitsField[mageDire->getX()][mageDire->getY()] = MAGEDIRE;
		
		units[0] = warriorRadiant;
		units[1] = archerRadiant;
		units[2] = mageRadiant;
		units[3] = warriorDire;
		units[4] = archerDire;
		units[5] = mageDire;
		
		unitsRect = new sf::RectangleShape();
    	unitsRect->setSize(sf::Vector2f(cellSize, cellSize));

		rectMapObj = new sf::RectangleShape();

		userInterface = new UserInterface();

		nodeShape = new sf::CircleShape();
		nodeShape->setRadius(8);

		
	}

	void handleEvent(sf::Event& e) {
		
			/*if (e.type == sf::Event::MouseWheelMoved) {
				if (cellSize > 10 && cellSize < 80) {
					cellSize += e.mouseWheel.delta;

				}
			}*/

			if (!cast) {
				if (e.type == sf::Event::MouseMoved) {

					
					if (camX <= 64 && e.mouseMove.x >= WINDOW_WIDTH - 50 && e.mouseMove.x <= WINDOW_WIDTH) { //�������� �������
						camX++;
					}

					if (camX > 0 && e.mouseMove.x >= 0 && e.mouseMove.x <= 50) {  //������� ������
						camX--;
					}

					if (camY > 0 && e.mouseMove.y >= 0 && e.mouseMove.y <= 50) { //������ �����
						camY--;
					}


					if (camY <= 100 && e.mouseMove.y <= 900 && e.mouseMove.y >= 850) {
						camY++;
					}


					if (selectedUnit != nullptr && !readyToGo) {

						currentCellX = (e.mouseMove.x) / cellSize + camX;
						currentCellY = (e.mouseMove.y) / cellSize + camY;

						path = world->findPath(
							selectedUnit->getX(),
							(selectedUnit->getY()),
							currentCellX, currentCellY,
							astar::HeuristicFunction::EUCLIDEAN,
							false);

					}
				}

			}
			else {
				if (e.type == sf::Event::MouseMoved) {
					meteorCurrentCellX = e.mouseMove.x / cellSize + camX; meteorCurrentCellY = e.mouseMove.y / cellSize + camY;
				//	std::cout <<"MeteorX is " << meteorCurrentCellX << "\n";
				//	std::cout << "MeteorY is "<< meteorCurrentCellY << "\n";
				}
			}
			

	

			if (e.type == sf::Event::MouseButtonReleased) {



				if (e.mouseButton.button == sf::Mouse::Button::Right) {
					selectedUnit = nullptr;
				}

				if (e.mouseButton.button == sf::Mouse::Button::Left) {

					if (!cast) {
						clickedCellX = e.mouseButton.x / cellSize + camX;
						clickedCellY = e.mouseButton.y / cellSize + camY;


						if (selectedUnit == nullptr) {
							for (int m = 0; m < AMOUNTOFUNITS; m++) {
								if (units[m]->getViability()) {
									if (clickedCellX == units[m]->getX() && clickedCellY == units[m]->getY() && playersTurn % 2 == 0 && units[m]->getTeam() == true) {
										this->hoodDraw = true;
										selectedUnit = units[m];
										userInterface->setSelectedUnit(selectedUnit);
										userInterface->setSelectedTower(mapObjects[0]);
										break;
									}
									else if (clickedCellX == units[m]->getX() && clickedCellY == units[m]->getY() && playersTurn % 2 == 1 && units[m]->getTeam() == false) {
										this->hoodDraw = true;
										selectedUnit = units[m];
										userInterface->setSelectedUnit(selectedUnit);
										userInterface->setSelectedTower(mapObjects[1]);
										break;
									}
								}
							}
						}
						else {
							if (path.size() > 0) {
								if (clickedCellX != selectedUnit->getX() && clickedCellY != selectedUnit->getY()) {

									for (int j = 0; j < AMOUNTOFUNITS; j++) {
										if (clickedCellX == units[j]->getX() && clickedCellY == units[j]->getY() && units[j]->getTeam() != selectedUnit->getTeam()) {
											selectedUnit2 = units[j];
											attackMovement = true;
										}
									}

									for (int q = 0; q < 2; q++) {
										if (clickedCellX >= mapObjects[q]->getX() && clickedCellX <= mapObjects[q]->getX() + 3
											&& clickedCellY >= mapObjects[q]->getY() && clickedCellY <= mapObjects[q]->getY() + 3) {
											attackTheTowerMovement = true;
											selectedTower = mapObjects[q];
										}
									}

									timerGoingTmp = path.size() - 1;
									readyToGo = true;
									timer = 0;
								}
							}
						}
					}

					
					if (cast) {

						meteorClickedCellX = e.mouseButton.x / cellSize + camX;
						meteorClickedCellY = e.mouseButton.y / cellSize + camY;

						float previousX = e.mouseButton.x;
						float previousY = e.mouseButton.y;

						float xCast = previousX;
						float yCast = previousY;

						if(xCast == previousX && yCast == previousY && e.mouseButton.y <= 700) {
							xCast = e.mouseButton.x;
							yCast = e.mouseButton.y;
							processMeteorCast(xCast , yCast);
							cast = false;
						}

					}	
				}
			}

			UI_EVENT_DATA uiData = userInterface->handleEvent(e);
			if (uiData.result == UI_EVENT_RESULT::ABILITY_USE) {
				if (uiData.data != nullptr) {
					int abilityId = *((int*)uiData.data);
					if (abilityId == ABILITY_METEOR) {
						cast = true;
					}
					//std::cout << "Ability with id " << abilityId << " being cast\n";
				}
				if (meteor == nullptr) {
					delete uiData.data;
				}
			}
		
	}

	void update(float dt) {

		timer += dt;
		
		if (damageDraw) {
			damageTimer += dt;
			if (damageTimer > 0.65) {
				damageDraw = false;
				damageTimer = 0;
			}
		}



		if (selectedUnit != nullptr && readyToGo) {
			processMovement();
		}
	    
		


		if (meteor != nullptr) {
			
			meteor->update(dt);
			
			if (meteor->isAnimationFinished()) {

				for (int i = 0; i < AMOUNTOFUNITS; i++) {
					if (units[i]->getX()  >= meteorClickedCellX - 1 && units[i]->getX()  <= meteorClickedCellX + 1 &&
						units[i]->getY()  >= meteorClickedCellY - 1 && units[i]->getY()  <= meteorClickedCellY + 1) {
						units[i]->setViability(false);
						if (units[i]->getTeam()) {
							amountOfChris--;
							if (amountOfChris == 0) {
								callback(8);
							}
						}
						else {
							amountOfBudd--;
							if (amountOfBudd == 0) {
								callback(9);
							}
						}
						
					}
				}

				meteorClickedCellX = 0;
				meteorClickedCellY = 0;
				delete meteor;
				meteor = nullptr;
				mouseMovementAllow = true;


			}
		}
		
		userInterface->update(dt);
		
	}


	void draw(sf::RenderWindow *window) {

		//����� ����
		for (int i = camX; i < camX + 32; i++) {
			for (int j = camY; j < camY + 24; j++) {
				rectField->setPosition(sf::Vector2f(cellSize * i + 10 - camX * cellSize, cellSize * j + 10 - camY * cellSize));

				if (fieldB[i][j] == 3) {
					rectField->setTexture(ResourceManager::sharedInstance().getTextureById(SAND), true);
				}

				if (fieldB[i][j] == 1) {
					rectField->setTexture(ResourceManager::sharedInstance().getTextureById(DIRT), true);
				}

				if (fieldB[i][j] == -1) {
					rectField->setTexture(ResourceManager::sharedInstance().getTextureById(WATER),true);
				}

				if (fieldB[i][j] == 6) {
					rectField->setTexture(ResourceManager::sharedInstance().getTextureById(WAY), true);
				}

				rectObstacle->setPosition(sf::Vector2f(cellSize * i + 10 - camX * cellSize, cellSize * j + 10 - camY * cellSize));

				if ((int)obstacles[i][j] == 0) {
					rectObstacle->setTexture(ResourceManager::sharedInstance().getTextureById(EMPTYO), true);
				}
				if ((int)obstacles[i][j] == 1) {
					rectObstacle->setTexture(ResourceManager::sharedInstance().getTextureById(ROCKO), true);
				}
				if ((int)obstacles[i][j] == 2) {
					rectObstacle->setTexture(ResourceManager::sharedInstance().getTextureById(WATERLILY), true);
				}
				if ((int)obstacles[i][j] == 3) {
					rectObstacle->setTexture(ResourceManager::sharedInstance().getTextureById(TUMBLEWEEDO), true);
				}
				if ((int)obstacles[i][j] == 4) {
					rectObstacle->setTexture(ResourceManager::sharedInstance().getTextureById(TREEO), true);
				}
				if ((int)obstacles[i][j] == 5) {
					rectObstacle->setTexture(ResourceManager::sharedInstance().getTextureById(QUICKSANDO), true);
				}
				window->draw(*rectField);
				window->draw(*rectObstacle);
				
			}
		}

		//����� �������� �����
		for (int l = 0; l < 3; l++) {
			rectMapObj->setPosition(sf::Vector2f((mapObjects[l]->getX() - camX)*cellSize  ,(mapObjects[l]->getY() - camY) * cellSize));
			if (mapObjects[l]->getType() == 2 ) {
				rectMapObj->setSize(sf::Vector2f(cellSize, cellSize));
				rectMapObj->setTexture(ResourceManager::sharedInstance().getTextureById(OBSIDIAN) , true);
			}
			else if(mapObjects[l]->getType() == 1) {
				rectMapObj->setSize(sf::Vector2f(cellSize * 3, cellSize * 3));
				rectMapObj->setTexture(ResourceManager::sharedInstance().getTextureById(CASTLERADIANT), true);
				castleAreaRect->setPosition(sf::Vector2f((mapObjects[l]->getX() - camX)*cellSize, (mapObjects[l]->getY() - camY) * cellSize));
				castleAreaRect->setFillColor(sf::Color(0, 255, 0, 128));
				window->draw(*castleAreaRect);
			}
			else {
				rectMapObj->setSize(sf::Vector2f(cellSize * 3, cellSize * 3));
				rectMapObj->setTexture(ResourceManager::sharedInstance().getTextureById(CASTLEDIRE), true);
				castleAreaRect->setPosition(sf::Vector2f((mapObjects[l]->getX() - camX)*cellSize, (mapObjects[l]->getY() - camY) * cellSize));
				castleAreaRect->setFillColor(sf::Color(0, 0, 255, 128));
				window->draw(*castleAreaRect);
			}
			if ((mapObjects[l]->getType() == 2 && drawTheRune == true) || mapObjects[l]->getType() == 1 || mapObjects[l]->getType() == 0) {
				window->draw(*rectMapObj);
			}
		}

		unitsRect->setSize(sf::Vector2f(cellSize, cellSize));
		//����� ������
		for (int i = 0; i < AMOUNTOFUNITS; i++) {
			if (units[i]->getViability()) {
				
				unitsRect->setPosition(sf::Vector2f((units[i]->getX() - camX) * cellSize, (units[i]->getY() - camY) * cellSize));
				unitsRect->setTexture(ResourceManager::sharedInstance().getTextureById(units[i]->getTextureIndex()), true);
				window->draw(*unitsRect);
			}
		}

		
		if (damageDraw) {
			window->draw(*damageIconRect);
		}

		if (!cast) {
			//����� ���� ������
			if (selectedUnit != nullptr) { //����� ���� �������
				if (path.size() > 0) {
					for (int l = path.size() - 1; l >= 0; l--) {
						astar::Node *node = path.at(l);
						//std::cout << "X: " << node->x << ", Y: " << node->y << ". COST: " << node->g << "\n";
					}
				}

				for (int l = path.size() - 1; l >= 0; l--) {
					astar::Node *node = path.at(l);
					nodeShape->setPosition((node->x - camX)* cellSize + (cellSize / 2), (node->y - camY) * cellSize + (cellSize / 2));
					if (selectedUnit->getMovePoints() >= node->g) {
						nodeShape->setFillColor(sf::Color::Green);
					}
					else {
						nodeShape->setFillColor(sf::Color::Red);
					}
					window->draw(*nodeShape);
				}
			}
		}
		else { //����� ��� ���������
			meteorAOErect->setPosition(sf::Vector2f((meteorCurrentCellX - camX)*cellSize,( meteorCurrentCellY  - camY)*cellSize));
			window->draw(*meteorAOErect);

		}


		if (meteor != nullptr) {
			meteorRect->setPosition(sf::Vector2f(meteor->getX() ,  meteor->getY() ));
			window->draw(*meteorRect);
		}

		userInterface->draw(window);

	}

	
	void processMeteorCast(float mouseButtonX, float mouseButtonY) {

		meteor = new Meteor(mouseButtonX, mouseButtonY);

	}	

	void processAttackAnimation(int damagedUnitX, int damagedUnitY) {
		
	}

	void processMovement() {
		mouseMovementAllow = false;
		if (readyToGo) {

			if (!attackMovement) {
				if (timerGoingTmp >= 0) {
					if (timer > 0.075) {
						astar::Node *node2 = path.at(timerGoingTmp);
						selectedUnit->setX(node2->x);
						selectedUnit->setY(node2->y);
						timerGoingTmp--;
						timer = 0;
					}

					if (timerGoingTmp == -1) {
						if (clickedCellX == mapObjects[2]->getX() && clickedCellY == mapObjects[2]->getY()) {
							selectedUnit->upgradeH();
							drawTheRune = false;
						}
						playersTurn++;
						selectedUnit = nullptr;
						timerGoingTmp = 0;
						timer = 0;
						readyToGo = false;
						mouseMovementAllow = true;

					}
				}
			}
			else if(attackMovement){
				if (timerGoingTmp >= 1) {
					if (timer > 0.075) {
						astar::Node *node2 = path.at(timerGoingTmp);
						selectedUnit->setX(node2->x);
						selectedUnit->setY(node2->y);
						timerGoingTmp--;
						timer = 0;
					}

					if (timerGoingTmp == 0) {
						damageIconRect->setPosition(sf::Vector2f((selectedUnit2->getX() + camX)*cellSize, (selectedUnit2->getY() + camY)*cellSize));
						damageDraw = true;
						selectedUnit2->setHP(selectedUnit->getDamage());
						
						if (selectedUnit2->getHPCurrent() <= 0) {
							if (selectedUnit2->getTeam()) {
								amountOfChris--;
								selectedUnit2->setViability(false);
								mouseMovementAllow = true;
								if (amountOfChris == 0) {
									buddhaVictory = true;
									callback(8);
								}
							}
							else {
								amountOfBudd--;
								selectedUnit2->setViability(false);
								mouseMovementAllow = true;
								if (amountOfBudd == 0) {
									jesusVictory = true;
									callback(9);
								}
							}
							
							
						}
						mouseMovementAllow = true;
						playersTurn++;
						selectedUnit = nullptr;
						selectedUnit2 = nullptr;
						timerGoingTmp = 0;
						timer = 0;
						readyToGo = false;
						attackMovement = false;
					}
				}
			}
			else if (attackTheTowerMovement) {
				if (timerGoingTmp >= 1) {
					if (timer > 0.075) {
						astar::Node *node2 = path.at(timerGoingTmp);
						selectedUnit->setX(node2->x);
						selectedUnit->setY(node2->y);
						timerGoingTmp--;
						timer = 0;
					}

					if (timerGoingTmp == 0) {
						damageIconRect->setPosition(sf::Vector2f((selectedUnit2->getX() + camX)*cellSize, (selectedUnit2->getY() + camY)*cellSize));
						damageDraw = true;
						selectedTower->setHP(selectedUnit->getDamage());

						if (!selectedTower->getTowersViability()) {
							if (selectedTower->getType() == 0) {
									buddhaVictory = true;
									callback(8);
								
							}
							else {
									jesusVictory = true;
									callback(9);
							}


						}
						mouseMovementAllow = true;
						playersTurn++;
						selectedUnit = nullptr;
						selectedUnit2 = nullptr;
						selectedTower = nullptr;
						timerGoingTmp = 0;
						timer = 0;
						readyToGo = false;
						attackMovement = false;
					}
				}





			}


		}
	}

	

};

