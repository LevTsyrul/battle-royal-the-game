#pragma once
#include "GameState.h"
#include "ResourceManager.h"

class MenuGameState : public GameState {
private:
	sf::Text *textM1 = new sf::Text(); //Continue
	sf::Text *textM2 = new sf::Text(); //Exit
	sf::Text *textM3 = new sf::Text(); //MENU AT THE TOP
	sf::Font *font = new sf::Font();   //�����
	sf::RectangleShape *rect;          //���
	sf::RectangleShape *rect2;
	sf::Texture *tex2;                 //���
	sf::Clock clock;
	float timer = 0;
	int t = 0;


public:
	~MenuGameState() {
		delete rect2;
		delete rect;
		delete textM1;
		delete textM2;
		delete textM3;
		delete tex2;
		delete font;
	}


	void init() {

		rect = new sf::RectangleShape(); //���
		rect->setPosition(sf::Vector2f(0, 0));	
		rect->setSize(sf::Vector2f(1600, 900));
		rect->setTexture(ResourceManager::sharedInstance().getTextureById(TEXBMain));

		textM1->setFont(*ResourceManager::sharedInstance().getFont());         //Cont
		textM1->setCharacterSize(100);
		textM1->setFillColor(sf::Color::White);
		textM1->setPosition(sf::Vector2f(650, 130));
		textM1->setString("Start");

		textM2->setFont(*ResourceManager::sharedInstance().getFont());         //Exit
		textM2->setCharacterSize(65);
		textM2->setFillColor(sf::Color::White);
		textM2->setPosition(sf::Vector2f(712, 678));
		textM2->setString("Exit");

		textM3->setFont(*ResourceManager::sharedInstance().getFont());         //Menu at the top
		textM3->setCharacterSize(36);
		textM3->setFillColor(sf::Color::Black);
		textM3->setPosition(sf::Vector2f(0, 300));
	}


	void handleEvent(sf::Event& e) {    //����� �������

		if (e.type == sf::Event::MouseMoved) {

			if (e.mouseMove.x >= 440 && e.mouseMove.x <= 1149 && e.mouseMove.y >= 134 && e.mouseMove.y <= 264) { //��������� �� ������ �����

				textM1->setFillColor(sf::Color::Green);
			}
			else {
				textM1->setFillColor(sf::Color::White);
			}



			if (e.mouseMove.x >= 572 && e.mouseMove.x <= 968 && e.mouseMove.y >= 675 && e.mouseMove.y <= 757){
				textM2->setFillColor(sf::Color::Red);
			}
			else {
				textM2->setFillColor(sf::Color::White);
			}
			

		}

		if (e.type == sf::Event::MouseButtonReleased) {
			if (e.mouseButton.button == sf::Mouse::Button::Left) {

				if (e.mouseButton.x >= 573 && e.mouseButton.x <= 968 && e.mouseButton.y >= 676 && e.mouseButton.y <= 757) {
					callback(-1);
				}

				if (e.mouseButton.x >= 441 && e.mouseButton.x <= 1149 && e.mouseButton.y >= 134 && e.mouseButton.y <= 264) {
					callback(1);
				}
			}
			
		}
	}

	void update(float dt) {
	
	}

	void draw(sf::RenderWindow *window) {
		window->draw(*rect);
		window->draw(*textM1);
		window->draw(*textM2);
		window->draw(*textM3);
	}
};