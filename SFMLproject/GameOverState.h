#pragma once
#include<SFML/Graphics.hpp>
#include "GameState.h"
#include "ResourceManager.h"
#include "Constants.h"
#include "MainGameState.h"



class GameOverState : public GameState {
private:
	sf::RectangleShape *gameOverStateRect;
public:

	void init() {
		gameOverStateRect = new sf::RectangleShape();
		gameOverStateRect->setPosition(sf::Vector2f(0, 0));
		gameOverStateRect->setSize(sf::Vector2f(WINDOW_WIDTH, WINDOW_HEIGHT));
		gameOverStateRect->setTexture(ResourceManager::sharedInstance().getTextureById(GOODVICTORY), true);
		
	}

	void handleEvent(sf::Event& e) {};
	void update(float dt) {}
	void draw(sf::RenderWindow *window) {
		window->draw(*gameOverStateRect);
	}


};