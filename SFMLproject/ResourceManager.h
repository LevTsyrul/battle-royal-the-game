#pragma once
#include<iostream>
#include <SFML/Graphics.hpp>
#include "astar.h"
#include "Constants.h"

//Singleton pattern
class ResourceManager {

private:
	sf::Texture *textures[TEX_COUNT];
	sf::Font *font;

	ResourceManager() {
		for(int i = 0; i < TEX_COUNT; i++){
			textures[i] = new sf::Texture();
		}

		font = new sf::Font();
		font->loadFromFile("12489.TTF");

		textures[SAND]->loadFromFile("images\\sand.jpg");
		textures[DIRT]->loadFromFile("images\\dirt.jpg");
		textures[WATER]->loadFromFile("images\\water.jpg");
		textures[TEXBMenu]->loadFromFile("images\\backgroundMain.png");
		textures[TEXBMain]->loadFromFile("images\\backgroundMenu.png");
		textures[BackBOX]->loadFromFile("images\\backBox.png");
		textures[WAY]->loadFromFile("images\\way.png");
		textures[WATERLILY]->loadFromFile("images\\waterlily.png");
		textures[ROCKO]->loadFromFile("images\\rock.png");
		textures[TUMBLEWEEDO]->loadFromFile("images\\tumbleweed.png");
		textures[TREEO]->loadFromFile("images\\tree.png");
		textures[QUICKSANDO]->loadFromFile("images\\quicksand.png");
		textures[EMPTYO]->loadFromFile("images\\emptyBack.png"); 
		textures[WARRIORRADIANT]->loadFromFile("images\\wariorRadiant.png");
		textures[ARCHERRADIANT]->loadFromFile("images\\archerRadiant.png");
		textures[MAGERADIANT]->loadFromFile("images\\mageRadiant.png");
		textures[TECHIESRADIANT]->loadFromFile("images\\bomberRadiant.png");
		textures[WARRIORDIRE]->loadFromFile("images\\warriorDire.png");
		textures[ARCHERDIRE]->loadFromFile("images\\archerDire.png");
		textures[MAGEDIRE]->loadFromFile("images\\mageDire.png");
		textures[TECHIESDIRE]->loadFromFile("images\\bomberDire.png");
		textures[HOODRADIANT]->loadFromFile("images\\radiantHud.jpg");
		textures[HOODDIRE]->loadFromFile("images\\direHud.jpg");
		textures[DAMAGEICON]->loadFromFile("images\\attack.png");
		textures[HEARTICON]->loadFromFile("images\\heart.png");
		textures[PYROBLAST]->loadFromFile("images\\pyroblast.png");
		textures[OBSIDIAN]->loadFromFile("images\\obsidian.png");
		textures[CASTLERADIANT]->loadFromFile("images\\castleRadiant.png");
		textures[CASTLEDIRE]->loadFromFile("images\\castleDire.png");
		textures[GOODVICTORY]->loadFromFile("images\\buddhaVictory.jpg");
		textures[EVILVICTORY]->loadFromFile("images\\jesusVictory.jpg");
		textures[METEOR]->loadFromFile("images\\meteor.png");
		textures[METEORSPELLICON]->loadFromFile("images\\meteorSpell.jpg");
		textures[ATTACKRECT]->loadFromFile("images\\attackRect.png");
		textures[INTERFACETOWERGOOD]->loadFromFile("images\\towerIconAtTheInterface.jpg");
		textures[INTERFACETOWEREVIL]->loadFromFile("images\\towerIconAtTheInterface2.jpg");
	};
	
	~ResourceManager() {
		for (int i = 0; i < TEX_COUNT; i++) {
			delete textures[i];
		}
		delete font;
	};

public:
	static ResourceManager& sharedInstance() {
		static ResourceManager instance;
		return instance;
	}

	sf::Texture* getTextureById(int id) {
		if (id >= TEX_COUNT || id < 0) {
			return nullptr;
		}
		return textures[id];
	}

	sf::Font* getFont(){
		return font;
	};
};