#pragma once
#include<iostream>

class MapObject {
private: 
	int x, y, type; 
	int amountHPMAX, currentHealthPoints;
	bool towersViability = true;
	//0 - ������ �����. 1 - ������ ���. 2 - ����
public:

	MapObject() {
		
	}

	MapObject(int x, int y, int type) {
		this->x = x;
		this->y = y;
		this->type = type;
		if (type == 0 || type == 1) {
			this->amountHPMAX = 200;
			this->currentHealthPoints = 200;
			towersViability = true;
		}
	}

	int getCurrentHP() {
		return this->currentHealthPoints;
	}

	int getHPMAX() {
		return this->amountHPMAX;
	}

	void setHP(int givenDamage) {
		this->currentHealthPoints -= givenDamage;
		if (this->currentHealthPoints <= 0) {
			this->towersViability = false;
		}
	}
	
	bool getTowersViability(){
		return this->towersViability;
	}

	int getX() {
		return this->x;
	}

	int getY() {
		return this->y;
	}

	int getType() {
		return this->type;
	}













};