#pragma once
#include "Unit.h"
																						
																																		
class Warrior : public Unit {

public:


	Warrior(bool answer) : Unit() {
		this->radiant = answer;
		this->hp_max = 111;
		this->hp_current = 100;
		this->movePoints_max = 500;
		this->movePoints_current = 0;
		this->damage = 40;
		this->viability = true;
		this->textureRadiant = WARRIORRADIANT;
		this->textureDire = WARRIORDIRE;
		this->index = 1;
	}


	void upgradeH() {
		if (this->upgrade) {
			this->damage = 100;
			this->hp_max = 300;
			this->hp_current = 300;
		}
	}


};