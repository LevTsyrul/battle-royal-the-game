#pragma once

#define WINDOW_WIDTH 1600
#define WINDOW_HEIGHT 900
#define FIELDSIZEX 100
#define FIELDSIZEY 100
#define AMOUNTOFUNITS 6
#define MOVEMENTSPEED 1
#define METEOR_SIZE 200
#define METEOR_ANIM_DURATION 2
#define ABILITY_METEOR 1

#define TEX_COUNT 36

#define DIRT 0 //dirt
#define SAND 1 //sand
#define WATER 2 //Water
#define TEXBMain 3 //��� �����
#define TEXBMenu 4 //��� ����
#define BackBOX 5 //��� ��������
#define WAY 6 //�������� ����
#define WATERLILY 7 //��������
#define ROCKO 8 //������
#define TUMBLEWEEDO 9 //��������
#define TREEO 10 //������
#define QUICKSANDO 11 //������� �����
#define EMPTYO 12 //������ png
#define WARRIORRADIANT 13 //���� �����
#define ARCHERRADIANT 14 //������ �����
#define MAGERADIANT 15 //��� �����
#define TECHIESRADIANT 16 //����� �����
#define WARRIORDIRE 17 //���� ���
#define ARCHERDIRE 18 //������ ���
#define MAGEDIRE 19 //��� ���
#define TECHIESDIRE 20 //����� ���
#define HOODRADIANT 21 //��� �����
#define HOODDIRE 22 //��� ���
#define DAMAGEICON 23 //����� �����
#define HEARTICON 24 //����� ��������
#define PYROBLAST 25 //���������
#define OBSIDIAN 26
#define CASTLERADIANT 27
#define CASTLEDIRE 28
#define GOODVICTORY 29
#define EVILVICTORY 30
#define METEOR 31
#define METEORSPELLICON 32
#define ATTACKRECT 33
#define INTERFACETOWERGOOD 34
#define INTERFACETOWEREVIL 35

