#pragma once
#include<iostream>

class Point {
private:

	int x, y, style;

public:

	Point(int x, int y, int style) {
		this->x = x;
		this->y = y;
		this->style = style;
	}
	Point(int x, int y) {
		this->x = x;
		this->y = y;
	}

	Point() {
		this->x = -1;
		this->y = -1;
	}

	int getStyle() {
		return this->style;
	}
	int getX() {
		return this->x;
	}

	int getY() {
		return this->y;
	}
};