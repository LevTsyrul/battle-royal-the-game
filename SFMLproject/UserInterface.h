#pragma once
#include<iostream>
#include "Unit.h"
#include "SFML/Graphics.hpp"
#include "ResourceManager.h"
#include<string>
#include "Constants.h"
#include "MapObjects.h"

enum UI_EVENT_RESULT {
	UNDEFINED,
	ABILITY_USE
};

struct UI_EVENT_DATA {
	UI_EVENT_RESULT result;
	void* data;

	UI_EVENT_DATA(UI_EVENT_RESULT result, void* data) {
		this->result = result;
		this->data = data;
	}
};

class UserInterface {
private:
	sf::RectangleShape* hoodItems;
	sf::Text *textDamage = new sf::Text();
	sf::Text *textHP = new sf::Text();
	sf::Text *textAboutTower = new sf::Text();
	Unit* selectedUnit = nullptr;
	MapObject* selectedTower = nullptr;
	
public:
	UserInterface() {
		textDamage->setFont(*ResourceManager::sharedInstance().getFont());         
		textDamage->setCharacterSize(48);
		textDamage->setFillColor(sf::Color::White);
		textDamage->setPosition(sf::Vector2f(400, 740));
		textDamage->setString("0 DAMAGE");

		textHP->setFont(*ResourceManager::sharedInstance().getFont());         
		textHP->setCharacterSize(48);
		textHP->setFillColor(sf::Color::White);
		textHP->setPosition(sf::Vector2f(400, 815));
		textHP->setString("0 HP");

		textAboutTower->setFont(*ResourceManager::sharedInstance().getFont());
		textAboutTower->setCharacterSize(48);
		textAboutTower->setFillColor(sf::Color::White);
		textAboutTower->setPosition(sf::Vector2f(850, 795));
		textAboutTower->setString("Tower's health points: 0/0");

		hoodItems = new sf::RectangleShape();
	}

	~UserInterface() {}

	UI_EVENT_DATA handleEvent(sf::Event& e) {
		//630 735
		if (e.mouseButton.x >= 630 && e.mouseButton.x <= 780 && e.mouseButton.y >= 735 && e.mouseButton.y <= 885) {
			int* param = new int();
			*param = ABILITY_METEOR;
			return UI_EVENT_DATA(UI_EVENT_RESULT::ABILITY_USE, param);
		}

		return UI_EVENT_DATA(UI_EVENT_RESULT::UNDEFINED, nullptr);
	}

	void update(float dt) {
	}

	void draw(sf::RenderWindow* window) {
		
		if (selectedUnit != nullptr) {
		
			//���
			hoodItems->setPosition(sf::Vector2f(0, 725));
			hoodItems->setSize(sf::Vector2f(1600, 200));
			if (selectedUnit->getTeam()) {
				hoodItems->setTexture(ResourceManager::sharedInstance().getTextureById(HOODRADIANT), true);
			}
			else {
				hoodItems->setTexture(ResourceManager::sharedInstance().getTextureById(HOODDIRE), true);
			}

			window->draw(*hoodItems);

			//��������
			hoodItems->setPosition(sf::Vector2f(50, 720));
			hoodItems->setSize(sf::Vector2f(200, 200));
			if (selectedUnit->getTeam()) {

				if (selectedUnit->getIndex() == 1) {
					hoodItems->setTexture(ResourceManager::sharedInstance().getTextureById(WARRIORRADIANT), true);
				}

				if (selectedUnit->getIndex() == 2) {
					hoodItems->setTexture(ResourceManager::sharedInstance().getTextureById(ARCHERRADIANT), true);
				}

				if (selectedUnit->getIndex() == 3) {
					hoodItems->setTexture(ResourceManager::sharedInstance().getTextureById(MAGERADIANT), true);
				}

			}
			else {

				if (selectedUnit->getIndex() == 1) {
					hoodItems->setTexture(ResourceManager::sharedInstance().getTextureById(WARRIORDIRE), true);
				}

				if (selectedUnit->getIndex() == 2) {
					hoodItems->setTexture(ResourceManager::sharedInstance().getTextureById(ARCHERDIRE), true);
				}

				if (selectedUnit->getIndex() == 3) {
					hoodItems->setTexture(ResourceManager::sharedInstance().getTextureById(MAGEDIRE), true);
				}
			}

			window->draw(*hoodItems);

			//�����
			hoodItems->setPosition(sf::Vector2f(300, 720)); 
			hoodItems->setSize(sf::Vector2f(70, 70));
			hoodItems->setTexture(ResourceManager::sharedInstance().getTextureById(DAMAGEICON), true);
			std::string s = std::to_string(selectedUnit->getDamage());
			textDamage->setString(s); 
			window->draw(*textDamage);
			window->draw(*hoodItems);

			//���������� ������
			hoodItems->setPosition(sf::Vector2f(300, 800)); 
			hoodItems->setSize(sf::Vector2f(70, 70));
			std::string tmpS = std::to_string(selectedUnit->getHPCurrent());
			std::string tmpS2 = std::to_string(selectedUnit->getHPMax());
			std::string s2 = tmpS + " / " + tmpS2;
			hoodItems->setTexture(ResourceManager::sharedInstance().getTextureById(HEARTICON), true);
			textHP->setString(s2);
			window->draw(*textHP);
			window->draw(*hoodItems);

			//797 787 ������� ��� �������� �� ������(����)
			//1507 837
			//TOWER'S HEALTH POINTS : 
			hoodItems->setPosition(sf::Vector2f(810, 780));
			hoodItems->setSize(sf::Vector2f(780, 80));
			if (selectedTower->getType() == 0) {
				hoodItems->setTexture(ResourceManager::sharedInstance().getTextureById(INTERFACETOWERGOOD), true);
			}
			else {
				hoodItems->setTexture(ResourceManager::sharedInstance().getTextureById(INTERFACETOWEREVIL), true);
			}
			std::string towersS1 = "TOWER'S HEALTH POINTS: ";
			std::string towersS2 = std::to_string(selectedTower->getCurrentHP());
			std::string towersS3 = "/";
			std::string towersS4 = std::to_string(selectedTower->getHPMAX());
			std::string towersSResult = towersS1 + towersS2 + towersS3 + towersS4;
			if (selectedUnit->getTeam()) {
				textAboutTower->setFillColor(sf::Color::Black);
			}
			else {
				textAboutTower->setFillColor(sf::Color::White);
			}
			textAboutTower->setString(towersSResult);
			window->draw(*hoodItems);
			window->draw(*textAboutTower);

			//����� ����
			if (selectedUnit->getIndex() == 3) {
				hoodItems->setPosition(sf::Vector2f(630, 735));
				hoodItems->setSize(sf::Vector2f(150, 150));
				hoodItems->setTexture(ResourceManager::sharedInstance().getTextureById(METEORSPELLICON), true);
				window->draw(*hoodItems);
			}

			

		}
	}

	void setSelectedUnit(Unit* unit) {
		selectedUnit = unit;
	}

	void setSelectedTower(MapObject* tower) {
		selectedTower = tower;
	}
};