#pragma once

#include<SFML/Graphics.hpp>
#include "GameState.h"
#include "ResourceManager.h"
#include "Constants.h"
#include "MainGameState.h"



class GameOverState2 : public GameState {
private:
	sf::RectangleShape *gameOverStateRect2;
public:

	void init() {
		gameOverStateRect2 = new sf::RectangleShape();
		gameOverStateRect2->setPosition(sf::Vector2f(0, 0));
		gameOverStateRect2->setSize(sf::Vector2f(WINDOW_WIDTH, WINDOW_HEIGHT));
		gameOverStateRect2->setTexture(ResourceManager::sharedInstance().getTextureById(EVILVICTORY), true);

	}

	void handleEvent(sf::Event& e) {};
	void update(float dt) {}
	void draw(sf::RenderWindow *window) {
		window->draw(*gameOverStateRect2);
	}

};