#pragma once
#include "Point.h"
#include "Constants.h"

class Meteor {
private:
	float x, y, startX, startY, endX, endY;
	float currentTime = 0;

public:

	Meteor(float mouseButtonX, float mouseButtonY) {
		startX = mouseButtonX - 100;
		startY = -METEOR_SIZE ;
		endX = mouseButtonX;
		endY = mouseButtonY;
	}

	float getEndX(){
		return this->endX;
	}

	float getEndY() {
		return this->endY;
	}
	void setX() {
		this->x = (endX )*(currentTime / METEOR_ANIM_DURATION);
		std::cout <<"x is " <<  this->x << "\n";
	}

	void setY() {
		this->y = (endY )*(currentTime / METEOR_ANIM_DURATION);
		std::cout <<"y is "<< this->y << " \n";
	}

	float getX() {
		return this->x;
	}

	float getY() {
		return this->y;
	}

	void update(float dt) {
		currentTime += dt;
		setX();
		setY();
	}

	bool isAnimationFinished() {
		return currentTime >= METEOR_ANIM_DURATION;
	}

};