#pragma once
#include "Unit.h"


class Archer : public Unit {


public:


	Archer(bool answer) : Unit() {
		this->index = 2;
		this->radiant = answer;
	    this->hp_max = 60;
		this->hp_current = 60;
		this->movePoints_max = 1000;
		this->movePoints_current = 0;
		this->damage = 55;
		this->viability = true;
		this->textureRadiant = ARCHERRADIANT;
		this->textureDire = ARCHERDIRE;
		this->upgrade = false;
	}

	void setUpgradedDMG() {
		this->damage = 120;
	}

	void upgradeH() {
		if (!this->upgrade) {
			setUpgradedDMG();
			this->upgrade = true;
		
		}
	}


};
