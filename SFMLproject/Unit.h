#pragma once	
class Unit {

protected:
	int x, y;
	int hp_max;
	int hp_current;
	int movePoints_max;
	int movePoints_current;
	int damage;
	bool viability;
	int range = 15;
	bool upgrade = false;
	int textureRadiant = -1;
	int textureDire = -1;
	bool radiant = true;
	int index = 0;

public:

	int getMovePoints() {
		return this->movePoints_max;
	}

	int getDamage() {
		return this->damage;
	}

	int getHPCurrent() {
		return this->hp_current;
	}

	int getHPMax() {
		return this->hp_max;
	}

	int getIndex() {
		return this->index;
	 }
	bool getTeam() {
		return this->radiant;
	}
      
	void setX(int x) {
		this->x = x;
	}

	void setY(int y) {
		this->y = y;
	}

	int getX() {
		return x;
	}

	int getY() {
		return this->y;
	}

	 void setHP(int takenDamage) {
		 this->hp_current -= takenDamage;
		 if (hp_current <= 0) {
			 this->viability = false;
		 }
	 };


	 bool getViability() {
		 return this->viability;
	 }

	 void setViability(bool viab) {
		 this->viability = viab;
	 }


	 void setMPCurrent(int doneMP) {
		 this->movePoints_current -= doneMP;
	 }

	 virtual void upgradeH() {
		 
	 }


	 int getTextureIndex() {
		 if (this->radiant) {
			 return textureRadiant;
		 }
		 return textureDire;
	 }

};

