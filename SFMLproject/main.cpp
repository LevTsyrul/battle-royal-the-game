#include "Constants.h"
#include "ResourceManager.h"
#include "MainGameState.h"
#include "MenuGameState.h"
#include "GameOverState.h"
#include "astar.h"
#include "GameOverState.h"
#include "GameOverState2.h"

extern void mainCallBack(int value);
GameState *activeGameState;
sf::RenderWindow *window;

void menuCallBack(int value) {

	if (value == 1) {
		delete activeGameState;
		activeGameState = new MainGameState();
		activeGameState->callback = &mainCallBack;
		activeGameState->init();
	}
	else if (value == -1) {
		window->close();
	}
}

void mainCallBack(int value){
	if (value == 2) {
		delete activeGameState;
		activeGameState = new MenuGameState();
		activeGameState->callback = &menuCallBack;
		activeGameState->init();
	} else if (value == 8) {
		delete activeGameState;
		activeGameState = new GameOverState();
		activeGameState->callback = &mainCallBack;
		activeGameState->init();
	} else if (value == 9) {
		delete activeGameState;
		activeGameState = new GameOverState2();;
		activeGameState->callback = &mainCallBack;
		activeGameState->init();
	}
}

int main()
{

	srand(time(NULL));
	window = new sf::RenderWindow(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Game");

	activeGameState = new MenuGameState();
	activeGameState->callback = &menuCallBack;
	activeGameState->init();
	sf::Clock clock;

	while (window->isOpen()) {
		sf::Event Evt;
		float dt = clock.restart().asSeconds();

		activeGameState->update(dt);

		while (window->pollEvent(Evt)) {
			if (Evt.type == sf::Event::Closed) {
				window->close();
			}
			activeGameState->handleEvent(Evt);
		}

		window->clear(sf::Color::Black);
		activeGameState->draw(window);
		window->display();
	}

	delete activeGameState;
	return 0;
}


	
