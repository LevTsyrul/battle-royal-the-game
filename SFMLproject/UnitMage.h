#pragma once
#include "Unit.h"

class Mage : public Unit {


	int pyroblastDMG;
	

public:

	Mage(bool answer) : Unit() {
		this->radiant = answer;
		this->hp_max = 90;
		this->hp_current = 90;
		this->movePoints_max = 500;
		this->damage = 70;
		this->viability = true;
		this->pyroblastDMG = 150;
		this->upgrade = true;
		this->textureRadiant = MAGERADIANT;
		this->textureDire = MAGEDIRE;
		this->index = 3;
	}
	
	int pyroblastCast() {
		return this->pyroblastDMG;
	}


	void upgradeH() {
		if (this->upgrade) {
			this->pyroblastDMG = 300;
		}
	}

};